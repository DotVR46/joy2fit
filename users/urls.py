from django.urls import path

from users.views import TrainerListView, TrainerDetail

urlpatterns = [
    path('trainers/', TrainerListView.as_view()),
    path('trainers/<uuid:pk>', TrainerDetail.as_view())
]