import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone

from .managers import FitUserManager


GENDER_CHOICES = (("male", "Мужской"), ("female", "Женский"), ("other", "Другое"))


def get_upload_path(instance, filename):
    # Файл загрузится в папку с именем пользователя
    if instance is not None:
        return "users/avatars/{0}/{1}".format(instance.username, filename)
    else:
        return "users/avatars"


class FitUser(AbstractUser):
    """
    Расширенная модель пользователя,
    включает в себя и User и Trainer
    """

    username = None
    id = models.UUIDField(
        verbose_name="Уникальный идентификатор",
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
    )
    email = models.EmailField(verbose_name="Email", unique=True)
    avatar = models.ImageField(
        verbose_name="Аватар", upload_to=get_upload_path, default="pic.jpg", blank=True
    )
    about = models.CharField(
        verbose_name="О себе", max_length=700, blank=True, null=True
    )
    date_joined = models.DateTimeField(
        verbose_name="Дата регистрации", default=timezone.now
    )
    gender = models.CharField(
        verbose_name="Пол", max_length=6, choices=GENDER_CHOICES, default="other"
    )
    birth_date = models.DateField(verbose_name="Дата рождения", blank=True, null=True)
    trainer = models.BooleanField(verbose_name="Статус тренера", default=False)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []  # email и password по умолчанию

    objects = FitUserManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        return "{0} {1}".format(self.first_name, self.last_name)

    class Meta:
        ordering = ("-date_joined",)
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"
