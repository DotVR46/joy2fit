from django.shortcuts import render
from django.views.generic import ListView, DetailView

from users.models import FitUser


class TrainerListView(ListView):
    """
    Список тренеров (активен, тренер)
    """
    queryset = FitUser.objects.filter(is_active=True, trainer=True)
    template_name = "users/trainer-list.html"


class TrainerDetail(DetailView):
    """
    Тренер по uuid (активен, тренер)
    """
    queryset = FitUser.objects.filter(is_active=True, trainer=True)
    template_name = "users/trainer-detail.html"
    context_object_name = "trainer"
