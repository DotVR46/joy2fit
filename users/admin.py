from django.contrib import admin

from users.models import FitUser


@admin.register(FitUser)
class FitUserAdmin(admin.ModelAdmin):
    list_display = (
        "email",
        "first_name",
        "last_name",
        "date_joined",
        "trainer",
        "is_active",
    )
    list_display_links = ("email",)
    list_editable = ("trainer", "is_active")
    list_filter = ("trainer", "is_active")
    search_fields = ("email", "first_name", "last_name")
