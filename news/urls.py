from django.urls import path

from news.views import NewsListView, NewsDetailView

urlpatterns = [
    path("news/", NewsListView.as_view()),
    path("news/<str:slug>", NewsDetailView.as_view()),
]
