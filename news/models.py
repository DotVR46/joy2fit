from PIL import Image
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.utils.text import slugify


class News(models.Model):
    title = models.CharField(verbose_name="Заголовок", max_length=300, db_index=True)
    slug = models.SlugField(verbose_name="Слаг", max_length=320, null=True, blank=True)
    content = RichTextUploadingField(
        verbose_name="Содержание",
    )
    created = models.DateTimeField(verbose_name="Создано", auto_now_add=True)
    published = models.BooleanField(verbose_name="Опубликовано", default=True)
    image = models.ImageField(
        verbose_name="Изображение",
        upload_to="news/images",
        null=True,
        default="pic.jpg",
    )

    def __str__(self):
        return self.title

    # Уменьшить изображение и добавить слаг
    def save(self, *args, **kwargs):
        self.slug = slugify(self.title) + '-' + str(self.id)
        super().save(*args, **kwargs)
        img = Image.open(self.image.path)
        if img.height > 500 or img.width > 500:
            img.thumbnail((500, 500))
            img.save(self.image.path)

    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости"
