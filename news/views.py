from django.views.generic import ListView, DetailView

from news.models import News


class NewsListView(ListView):
    """
    Список опубликованных новостей
    """
    queryset = News.objects.filter(published=True)
    template_name = "news/news-list.html"


class NewsDetailView(DetailView):
    """
    Одна новость по slug
    """
    queryset = News.objects.filter(published=True)
    template_name = "news/news-detail.html"
    context_object_name = "news"
